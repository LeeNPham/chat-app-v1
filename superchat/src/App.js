import React, { useState } from 'react';
import './App.css';

//update imports to be stored under compat
import firebase from 'firebase/compat/app'; //firebase SDK
import 'firebase/compat/firestore'; //firestore for DB
import 'firebase/compat/auth'; // for user authentication

// hooks to work with firebase and react
import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore';

//to initialize our app
firebase.initializeApp({
  //your config
  apiKey: "AIzaSyCk8n8gH11aw916eie7s-Y7lBSCjjUof8U",
  authDomain: "chat-app-react-and-firebase.firebaseapp.com",
  projectId: "chat-app-react-and-firebase",
  storageBucket: "chat-app-react-and-firebase.appspot.com",
  messagingSenderId: "739849726768",
  appId: "1:739849726768:web:bc97efd47e73b8854676f5",
  measurementId: "G-3KXREN8YGK"
})

// reference to auth and store SDKs as variables
const auth = firebase.auth();
const firestore = firebase.firestore();


function App() {
  const [user] = useAuthState(auth)
  return (
    <div className="App">

      <header>
        <h1>⚛️🔥💬</h1>
        <SignOut />
      </header>

      <section>
        {user ? <ChatRoom /> : <SignIn />}
      </section>

    </div>
  );
}


function SignIn() {
  const signInWithGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider);
  }

  return(
    <>
      <button className="sign-in" onClick={signInWithGoogle}>Sign in with Google</button>
      <p>Do not violate the community guidelines or you will be banned for life!</p>
    </>
  )
}

function SignOut() {
  return auth.currentUser && (
    <button className="sign-out" onClick={() => auth.signOut()}>Sign Out</button>
  )
}

function ChatRoom() {
  // const dummy = useRef();
  const messagesRef = firestore.collection('messages');
  const query = messagesRef.orderBy('createdAt').limitToLast(25);

  // listen to data with a hook (useCollectionData)
  const [messages] = useCollectionData(query, {idField: 'id'});

  // adding a stateful value to our component with a useState hook and have it start as an empty string
  const [formValue, setFormValue] = useState('');

  // event handler called sendMessage for form submission
  const sendMessage = async (e) => {
    //prevents page from refreshing after a submission
    e.preventDefault();
    const { uid, photoURL } = auth.currentUser;

    //create new document in firestore
    await messagesRef.add({
      text: formValue,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      uid,
      photoURL
    });
    // reset form value back to an empty string after sending a message
    setFormValue('');
  }

  return (
    <>
      <main>
        {messages && messages.map(msg => <ChatMessage key={msg.id} message={msg}/>)}
      </main>

      {/* write value to firestore */}
      <form onSubmit={sendMessage}>

        {/* input tag for value we want the user to create(I.E a message) */}
        <input value={formValue} onChange={ (e) => setFormValue(e.target.value)} placeholder="say something nice" />
        {/* button to submit the form */}
        <button type="submit">🕊️</button>
        {/* alternate version: <button type="submit" disabled={!formValue}>🕊️</button> */}

      </form>
    </>
  )
}

// child component
function ChatMessage(props) {
  const { text, uid , photoURL} = props.message;
  //conditional css using a turnery operator
  const messageClass = uid === auth.currentUser.uid ? 'sent' : 'received';

  return (
    <>
    <div className={`message ${messageClass}`}>
      <img src={ photoURL } alt=""/>
      <p>{text}</p>
    </div>
    </>
  )
}

export default App;
